﻿:: Run Cleanup
call:cleanup
tskill acrord32 
pdflatex -quiet bp.tex
bibtex bp
pdflatex -quiet bp.tex
pdflatex -quiet bp.tex
call:cleanup
START "" bp.pdf

:cleanup
:: del *.log
del *.dvi
del *.aux
del *.bbl
del *.blg
del *.brf
del *.out
del *.lof
del *.log
del *.lot
del *.toc
goto:eof
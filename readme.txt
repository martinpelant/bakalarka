﻿Obsah tohoto CD:
|	readme.txt......................................stručný popis obsahu CD
|---apk ....................... adresář se spustitelnou formou implementace
|---src
|	|---impl.....................................zdrojové kódy implementace
|	|---thesis .......................zdrojová forma práce ve formátu LATEX
|---text 
	|---thesis.pdf ...............................text práce ve formátu PDF
	
	
Návod na nainstalování aplikace:
Po stáhnutí Android SDK a nainstalování potřebných ovladačů je možné mobilní zařízení připojit k počítači pomocí USB kabelu a nainstalovat aplikaci pomocí příkazu:

adb install .\rman-an.apk

Tento příkaz předpokládá, že máte v aktuálním adresáři umístěný soubor rman-an.apk, který najdete na tomto CD ve složce apk a zárověň že máte v systémové proměnné PATH složku platform-tools z Android SDK a nebo se v této složce nacházíte.

Návod na nainstalování potřebných ovladačů a Android SDK najdete na této stránce
http://developer.android.com/sdk/win-usb.html#WinUsbDriver


Další možností je soubor rman-an.apk dostat na externí paměť zařízení a pomocí správce souborů (například ES File manager - https://play.google.com/store/apps/details?id=com.estrongs.android.pop) daný soubor otevřít a nainstalovat.

Další možností je si tento soubor poslat emailem, či jiným způsobem nasdílet do zařízení a opět nainstalovat.
